var args = arguments[0] || {};

Ti.API.info(args);

var bbdd = require('bbdd');

var aux = 0;
var totalPrice = Number(args.priceDiscount) + Number(args.shippingPrice);

var delivery_code;
var request_id;

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Resumen del pedido"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);

	$.deliveryA.setText(args.house);
	$.totalPrice.setText(totalPrice.toFixed(2));
	$.observationText.setText(args.comments);
	
	setOrderRow(order);
	new bbdd({},
        'vendors/'+ order[0].vendor_id, function(data){
        $.localName.setText(data.vendors.name);
    }, 'GET');
	
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	winClose.push(next);
	next.open();
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		left : "100%"
	});
});

$.bottom.addEventListener('singletap', function(){
	
	if($.visaTick.color == Alloy.CFG.appColor || $.cashTick.color == Alloy.CFG.appColor){
		
		if($.cashTick.color == Alloy.CFG.appColor){
		    var payment = "E";
		}else{
		    var payment = "T"; 
		}
		
		if($.giftTick.color == Alloy.CFG.appColor4){
		    var giftSelect = 1;
		}else{
		    var giftSelect = 0;
		}
		
		if(giftSelect == 1 && payment == "E"){
			alert('Los regalos deben pagarse con tarjeta');
			
		} else {
		
			$.indicator.show();
			$.bottom.hide();
			
			new bbdd({
					address: args.address_id,
					vendor: order[0].vendor_id,
					pay_method: payment,
					comments: args.comments,
					is_gift: giftSelect,
					discount_code: args.code_discount
				}, 
				'orders/'+ Ti.App.Properties.getObject('user').id, 
				function(data) {
					
					if(data == null || data == undefined){
						var a = Titanium.UI.createAlertDialog({
						    title: 'Oops!',
						    message: 'Algo ha fallado, por favor inténtalo de nuevo.'
						});
						a.show();
						$.indicator.hide();
						$.bottom.show();
					} else{
				
					request_id = data.order.id;
					delivery_code = data.order.delivery_code;
					
					var o = order;
					
					Ti.API.info(o);
					
					aux = 0;
					
					for (var i in o) {
						new bbdd({
							product: o[i].product_id,
							quantity: o[i].quantity
						}, 'orders/'+ Ti.App.Properties.getObject('user').id +'/lines/'+ request_id, function() {
							aux ++;
							if (aux == o.length) {
								new bbdd({}, 'orders/'+ request_id +'/start', function(data) {
									Ti.API.info(data);
									$.indicator.hide();
									$.bottom.show();
									Ti.API.info('entra en start');
									
									if(payment == "E"){
										var next = Alloy.createController("thanks", {delivery_code : delivery_code});
										next = next.getView();
										winClose.push(next);
										next.open();
										next.animate({left : 0});
									} else {
										//www.warever.es/pay?id=ID_PEDIDO
										var next = Alloy.createController("payment", {order_id : request_id, delivery_code : delivery_code });
										next = next.getView();
										winClose.push(next);
										next.open();
										next.animate({left : 0});
									}
									
								}, 'GET');
							}
						});
					}	
				}
			}); 
		}
	}else{
		alert("Debes indicarnos cómo quieres pagar.");
	}
		
});

$.cancel.addEventListener('singletap', function(){
	order = [];
	winCloseFunction();
	$.win.close(); 
	$.win.animate({
		left : "100%"
	});
});


function setOrderRow(order){
	for (var i = 0; i < order.length; i++) {
		
		var row = Ti.UI.createLabel({
			height : "20dp",
			width: "96%",
			left : "2%",
			font : {
				fontFamily : Alloy.CFG.regularFont,
				fontSize : "14dp"
			}, 
			text : order[i].product_name + ' ('+ order[i].quantity + ')',
			color : Alloy.CFG.appColor2
		});
			
		$.container.add(row);
	}
}

//Selection ticks
$.selectPayment.addEventListener('click', function(e){
	if(e.source == $.visaSel || e.source == $.visaTick || e.source == $.visa){
		$.visaTick.color = Alloy.CFG.appColor;
		$.cashTick.color = Alloy.CFG.appColor5;
	}else{
		$.visaTick.color = Alloy.CFG.appColor5;
		$.cashTick.color = Alloy.CFG.appColor;
	}
});

$.giftView.addEventListener('click', function(e){
	if($.giftTick.color == Alloy.CFG.appColor5){
        $.giftTick.color = Alloy.CFG.appColor4;
        $.giftYes.color = Alloy.CFG.appColor4;
    }else{
        $.giftTick.color = Alloy.CFG.appColor5;
        $.giftYes.color = Alloy.CFG.appColor5;
    }
});