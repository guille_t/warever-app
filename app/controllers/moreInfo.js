var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

var hours = [];

function init() {
    $.header.setLeftButton("\ue60f");
    $.header.leftButtonImg.setColor("white");
    $.header.setTitle("Más información");
    $.header.setTitleImage(undefined);
    $.header.setRightButton(undefined);
    $.header.setBackgroundColor(Alloy.CFG.appColor);
    $.header.headerTitle.setColor("white");
    
    $.indicator.show();
    
    $.nameLocal.setText(args.vendor.name);
    $.placeLocal.setText(args.vendor.address);
    $.minText.setText(args.vendor.avg_price.toFixed(2));
    $.typeText.setText(args.categoryRest);
    
    if(args.vendor.is_open == 1){
    	$.status.setText("\ue625");
    	$.status.setColor("#00b369");
    	$.statusText.setText("Abierto");
    	$.statusText.setColor("#00b369");
    } else {
    	$.status.setText("\ue622");
    	$.status.setColor("#d83e32");
    	$.statusText.setText("Cerrado");
    	$.statusText.setColor("#d83e32");
    }
    
     new bbdd({},
        'vendors/'+ args.vendor.id +'/hours', function(data){
        	setHours(data.hours);
    }, 'GET');
   
}

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({left : "100%"});
});

function setHours(hours){
	
	var days = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado", "Domingo"];
	
	
	for(var i in hours){
        
        var day = Ti.UI.createLabel({
            left : "46dp",
            width : "30%",
            font : {
                fontFamily : Alloy.CFG.boldFont,
                fontSize : "14dp"
            }, 
            text : days[i],
            color : Alloy.CFG.appColor
        });   
        
        var right = Ti.UI.createView({
        	bottom : 0,
        	height : "95%",
            width : "40%",
            right : "20dp"
        });
        
         var row = Ti.UI.createTableViewRow({
            height : "60dp"
         });
        
        //HoursLines
        var morning = Ti.UI.createLabel({
            width : "100%",
            height : "49%",
            font : {
                fontFamily : Alloy.CFG.regularFont,
                fontSize : "14dp"
            }, 
            text : hours[i].morning_from + ' a ' + hours[i].morning_to,
            color : Alloy.CFG.appColor3
        });
        
        if(hours[i].evening_from){
        	
        	morning.setTop(0);
        	
	        var evening = Ti.UI.createLabel({
	            bottom : 0,
	            width : "100%",
	            height : "49%",
	            font : {
	                fontFamily : Alloy.CFG.regularFont,
	                fontSize : "14dp"
	            }, 
	            text : hours[i].evening_from + ' a ' + hours[i].evening_to,
	            color : Alloy.CFG.appColor3
	        });
	        
	        right.add(evening);
        }

        row.add(day);
        row.add(right);
        right.add(morning);
        
        $.timeTable.appendRow(row);
		$.timeTable.setHeight(Ti.UI.SIZE);
	}

	$.indicator.hide();
}
