var args = arguments[0] || {};

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Direcciones de entrega"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
    
    new bbdd({},
	    'customers/'+ Ti.App.Properties.getObject('user').id + '/addresses', function(data){
	    	setOtherAddresses(data.addresses);
	}, 'GET');
}

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		left : "100%"
	});
});

$.add.addEventListener('singletap', function(){
	var next = Alloy.createController("newDeliveryA", {callback: setNewAddress});
    next = next.getView();
   	next.open();
    next.animate({top : 0});
});

//setOtherAddresses
function setOtherAddresses(addresses){
	for(var i in addresses){
		var row = Ti.UI.createTableViewRow({
			top : "1%",
			height : "34dp",
			backgroundColor : Alloy.CFG.appColorBackground,
			_data : addresses[i]
		});
		var icon = Ti.UI.createLabel({
			left : 0,
			width : "15%",
			text : "\ue60b",
			color : Alloy.CFG.appColor,
			font : {
				fontFamily: Alloy.CFG.iconFont,
				fontSize : "22dp"
			},
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER
		});
		var text = Ti.UI.createLabel({
			width : "70%",
			text : addresses[i].address + " " + addresses[i].number + ", " + addresses[i].postal_code + " " + addresses[i].city,
			color : Alloy.CFG.appColor6,
			font : {
				fontFamily: Alloy.CFG.regularFont,
				fontSize : "14dp"
			}
		});	
		var deleteIcon = Ti.UI.createLabel({
			right : 0,
			width : "15%",
			text : "\ue60f",
			color : Alloy.CFG.appColor5,
			font : {
				fontFamily: Alloy.CFG.iconFont,
				fontSize : "22dp"
			},
			textAlign : "center"
		});
		
		deleteIcon.addEventListener('singletap', function(e){
			 new bbdd({},
			    'customers/'+ Ti.App.Properties.getObject('user').id + '/addresses/' + addresses[i].id + '/delete', function(e){
			    	$.table.deleteRow(row);
			}, 'GET');
		});
		
		row.add(icon);
		row.add(text);
		row.add(deleteIcon);
		// row._address = addresses[i].address + ", " + addresses[i].postal_code  + ' ' + addresses[i].city;
		// row._address_id = addresses[i].id;
		// row._address_cp = addresses[i].postal_code;
	    $.table.insertRowBefore($.table.sections[0].rows.length - 1, row);
	   
	  }
}

//Set new address
function setNewAddress(params) {
	newAddress = params;
	setRow(newAddress);
}

function setRow(newAddress){
	var row = Ti.UI.createTableViewRow({
		top : "1%",
		height : "34dp",
		backgroundColor : Alloy.CFG.appColorBackground
	});
	var icon = Ti.UI.createLabel({
		left : 0,
		width : "15%"
	});
	var text = Ti.UI.createLabel({
		width : "70%",
		text : newAddress.street + ", " + newAddress.post + ' ' + newAddress.city,
		color : Alloy.CFG.appColor6,
		font : {
			fontFamily: Alloy.CFG.regularFont,
			fontSize : "14dp"
		}
	});	
	var deleteIcon = Ti.UI.createLabel({
		id : "tick",
		right : 0,
		width : "15%",
		text : "\ue60f",
		color : Alloy.CFG.appColor5,
		font : {
			fontFamily: Alloy.CFG.iconFont,
			fontSize : "22dp"
		},
		textAlign : "center"
	});
	
	deleteIcon.addEventListener('singletap', function(e){
		 new bbdd({},
		    'customers/'+ Ti.App.Properties.getObject('user').id + '/addresses/' + addresses[i].id + '/delete', function(e){
		    	$.table.deleteRow(row);
		}, 'GET');
	});
	
	row.add(icon);
	row.add(text);
	row.add(deleteIcon);
	row._address = newAddress.street + ", " + newAddress.city + ' ' + newAddress.post;
	row._address_id = newAddress.address_id;
	row._address_cp = newAddress.post;
    $.table.insertRowBefore($.table.sections[0].rows.length - 1, row);
}
