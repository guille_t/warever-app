var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

function init(){
	
	check();

}

function check(){
	if (Ti.App.Properties.getObject('user')) {
		if (!args.win1) args.win1 = null;
		if (!args.win2) args.win2 = null;
		if (!args.win3) args.win3 = null;
		if (!args.win4) args.win4 = null;
		var next = Alloy.createController("profile", {fInit: args.fInit});
		next = next.getView();
		winClose.push(next);
		next.open({left: 0});
		$.win.addEventListener('open', function() {
			$.win.close();
		});
	}
}

$.closeButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});

$.shadow.addEventListener('singletap', function() {
	if ($.mailField.value == '' || $.passField.value == '') {
		alert("Debes rellenar los dos campos");
		return;
	}
	
	new bbdd({email: $.mailField.value, password: $.passField.value}, 'login', function(data) {
		
		if(data == null){
			alert('Los datos de acceso no son correctos');
		}else{
		Ti.App.Properties.setObject('user', data.user);
		Ti.App.Properties.setString('token', data.token);
		
		// new bbdd({}, 'customers/'+Ti.App.Properties.getObject('user').id+'/addresses', function(data) {
			// Ti.App.Properties.setObject('addresses', data);
		// });
		
		//alert('¡Bienvenido '+ Ti.App.Properties.getObject('user').nombre +'!');
		var a = Titanium.UI.createAlertDialog({
		    title:'¡Bienvenido!',
		    message:'Hola de nuevo '+Ti.App.Properties.getObject('user').nombre
		});
		a.show();

		$.win.close();
		$.win.animate({top : "100%"});
		}
	});
});

$.forgot.addEventListener('singletap', function(){
	Titanium.Platform.openURL(Alloy.CFG.urlPassForgoten);
	// var webview = Titanium.UI.createWebView({url:Alloy.CFG.urlPassForgoten});
    // var window = Titanium.UI.createWindow();
    // window.add(webview);
    // window.open({modal:true});
});

$.create.addEventListener('singletap', function(){
	var next =Alloy.createController("create");
	next = next.getView();
	winClose.push(next);
	next.open();
	next.animate({top : 0});
});