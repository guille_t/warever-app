var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

function init() {
    $.header.setLeftButton("\ue60f");
    $.header.setTitle("Otra dirección");
    $.header.setTitleImage(undefined);
    $.header.setLeftButtonColor("#FFFFFF");
    $.header.setBackgroundColor(Alloy.CFG.appColor4);
    $.header.setRightButtonText("Hecho");
    $.header.setRightButton(undefined);
    $.header.setRightButtonColor("#FFFFFF");
}

var leftButton = $.header.getLeftButton();
var rightButton = $.header.getRightButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});

rightButton.addEventListener('singletap', function() {
	
	rightButton.visible = false;
	
	if ($.newStreetT.value == '' || $.newNumberT.value == '' || $.newCityT.value == '' || $.newPostT.value == '') {
		alert("Existen campos obligatorios, por favor, rellenalos.");
		rightButton.visible = true;
		return;
	}
	
	var house = {
		street: $.newStreetT.value,
		number: $.newNumberT.value,
		city: $.newCityT.value,
		post: $.newPostT.value,
		address_id : ''
	};
	
	new bbdd({
		address: house.street, 
		number: house.number,
		postal_code: house.post, 
		city: house.city, 
		name: Ti.App.Properties.getObject('user').nombre, 
		phone: Ti.App.Properties.getObject('user').phone
		}, 
		'customers/'+ Ti.App.Properties.getObject('user').id +'/addresses', 
		function(data) {
			
			if(data == null) {
				var a = Titanium.UI.createAlertDialog({
					    title: '¡Uups!',
					    message: 'Error de conexión, inténtalo de nuevo o ponte en contacto con Warever.'
				});
				a.show();
				rightButton.visible = true;
				
			} else {
				
				house.address_id = data.address.id;
				
				args.callback(house);
				$.win.close();
				$.win.animate({
					top : "100%"
				});
				rightButton.visible = true;
			}
		}
	);
});
