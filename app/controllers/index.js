 if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
   Titanium.API.info(' no connection ');
   alert('Warever necesita que tengas acceso a internet para funcionar');
   
} else {
   Titanium.API.info(' connection present ');
    $.index.open({
        left : 0
    });
    var nextInfo;
    init();
}

function init() {
	new bbdd({},
        'sectors', function(data){
        sectors = data.sectors;
        nextInfo = sectors[0];
    }, 'GET');

    check();
    $.header.setTitleImage("/images/warever2.png");
    
    $.bot.visible = true;
    $.shadowWare.visible = false;
    $.messageText.visible = false;
    
    $.foods.backgroundColor = Alloy.CFG.appColor4;
}

Alloy.Globals.mainInit = init;

$.index.addEventListener('focus', check);

function check() {
	
    if (Ti.App.Properties.getObject('user')) {
        $.header.setRightButton("\ue60a");
        $.header.setRightButtonText(undefined);

    } else {
        $.header.setRightButton(undefined);
        $.header.setRightButtonText("Login");
    }

}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function() {
    var next = Alloy.createController("login", {
        fInit : init
    });
    next = next.getView();
    next.open();
    next.animate({ top : 0 });
});

$.foods.addEventListener('singletap', function() {
	 nextInfo = sectors[0]; 
	 $.bot.visible = true;
	 $.messageText.visible = false;
     $.shadowWare.visible = false;  
     $.mid.setBackgroundImage("/images/C1.jpg");  
     $.selectionText.setText("¿Qué te apetece hoy?");    
     $.foods.backgroundColor = Alloy.CFG.appColor4;
     $.shops.backgroundColor = "transparent";
     $.messages.backgroundColor = "transparent";
     $.warever.backgroundColor = "transparent";
});

$.shops.addEventListener('singletap', function() {
	 nextInfo = sectors[1];
	 $.bot.visible = true;
	 $.messageText.visible = false;
     $.shadowWare.visible = false;  
     $.mid.setBackgroundImage("/images/C2.jpg");  
     $.selectionText.setText("¡Tus tiendas favoritas para ti!");     
     $.foods.backgroundColor = "transparent";
     $.shops.backgroundColor = Alloy.CFG.appColor4;
     $.messages.backgroundColor = "transparent";
     $.warever.backgroundColor = "transparent";
});

$.messages.addEventListener('singletap', function() {
	nextInfo = 4;
	 $.bot.visible = false;
	 $.messageText.visible = true;
     $.shadowWare.visible = false;
     $.mid.setBackgroundImage("/images/C3.jpg"); 
     $.selectionText.setText("Tu entregas dónde y cuándo quieras"); 
     $.foods.backgroundColor = "transparent";
     $.shops.backgroundColor = "transparent";
     $.messages.backgroundColor = Alloy.CFG.appColor4;
     $.warever.backgroundColor = "transparent";
});

$.warever.addEventListener('singletap', function() {
	 $.bot.visible = false;
	 $.messageText.visible = false;
     $.shadowWare.visible = true;
     $.mid.setBackgroundImage("/images/C4.jpg");
     $.selectionText.setText("Te regalamos tiempo");  
     $.foods.backgroundColor = "transparent";
     $.shops.backgroundColor = "transparent";
     $.messages.backgroundColor = "transparent";
     $.warever.backgroundColor = Alloy.CFG.appColor;
});

function validatePostalCode(pc){
	  var postalFormat = /^\d{5}$/;
	  
	  if(pc.match(postalFormat)){
		   return true;        
	  } else {
		   return false;
	  }
}

$.shadow.addEventListener('singletap', function(e) {
	var postal_code = $.postalField.value;
	
	function validatePostalCode(pc){
		  var postalFormat = /^\d{5}$/;
		  
		  if(pc.match(postalFormat)){
			   return true;        
		  } else {
			   return false;
		  }
	}
	
	if(!validatePostalCode(postal_code))  {
	    alert("El código postal introducido no es correcto");
	    return; 
	 } else {
		var next = Alloy.createController("restaurantListImages", {postal_code: $.postalField.value, sector : nextInfo});
	    next = next.getView();
	    next.open();
	    winClose.push(next); 
	    next.animate({ left : 0 });
	}
});

$.shadowWare.addEventListener('singletap', function(e) {
    var next = Alloy.createController("ask1");
    next = next.getView();
    winClose.push(next);
    next.open();
    next.animate({ left : 0 });
});