var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Registro"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton(undefined);
    $.header.setRightButtonText("Hecho");
    
}

var leftButton = $.header.getLeftButton();
var rightButton = $.header.getRightButton();


leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});

rightButton.addEventListener('singletap', function() {
	
	$.indicator.show();
	rightButton.hide();
	
	var user = {
		name: $.nameField.value,
		email: $.mailField.value,
		password: $.passField.value,
		phone : $.telfField.value,
		address: $.streetField.value,
		number: $.streetNumberField.value,
		floor: $.streetFloorField.value,
		postal_code: $.postalField.value,
		city : $.cityField.value,
		dni : $.dniField.value
	};

	function validateEmail(em)  {  
		var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
		
		if(em.match(mailFormat))  {  
			return true;  
		} else {   
			return false; 
		}  
	}  

	function validatePhone(ph){
		  var phoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
		  
		  if(ph.match(phoneNum)){
			   return true;        
		  } else {
			   return false;
		  }
	}
	
	function validatePostalCode(pc){
		  var postalFormat = /^\d{5}$/;
		  
		  if(pc.match(postalFormat)){
			   return true;        
		  } else {
			   return false;
		  }
	}

	if (user.name == '' || user.email == '' || user.password == '' || user.phone == '' 
		|| user.address == '' || user.postal_code == '' || user.city == '') {
		alert("Existen campos son obligatorios, por favor, rellenalos.");
		
		$.indicator.hide();
		rightButton.show();
		
		return;
	} 
	
	if(!validateEmail(user.email))  {
	    alert("El email introducido no es correcto");
	    $.mailField.setColor("red");
	    $.mailField.focus();
	    
	    $.indicator.hide();
		rightButton.show();
		
	    return;    
	}else {
		$.mailField.setColor(Alloy.CFG.appColor6);
	} 
	
	if(!validatePhone(user.phone))  {
	    alert("El teléfono introducido no es correcto");
	    $.telfField.setColor("red");
	    $.telfField.focus();
	    
	    $.indicator.hide();
		rightButton.show();
		
	    return; 
	}else {
		$.telfField.setColor(Alloy.CFG.appColor6);
	}
	
	if(!validatePostalCode(user.postal_code))  {
	    alert("El código postal introducido no es correcto");
	    $.postalField.setColor("red");
	    $.postalField.focus();
	    
	    $.indicator.hide();
		rightButton.show();
		
	    return; 
	 }else {
		$.postalField.setColor(Alloy.CFG.appColor6);
	}
	
	new bbdd({
		name: user.name,
		email: user.email,
		password: user.password,
		phone : user.phone,
		address: user.address,
		floor: user.floor,
		number: user.number,
		postal_code: user.postal_code,
		city : user.city,
		dni : user.dni
		}, 
		'customers/', 
		function(data) {
			
			if (data==null){
				
				alert('Lo sentimos, error inesperado, inténtalo de nuevo por favor.');
				
				$.indicator.hide();
				rightButton.show();
		
			} else if(data.message == 'El DNI no es correcto'){
				alert('El DNI no es correcto');
				$.dniField.setColor("red");
				$.indicator.hide();
				rightButton.show();
				
			}  else if(data.message == 'El email ya existe'){
				alert('El email ya existe');
				$.mailField.setColor("red");
				$.indicator.hide();
				rightButton.show();
				
			}else{
				Ti.App.Properties.setObject('user', data.user);
				Ti.App.Properties.setString('token', data.token);
				
				alert('¡Tu cuenta ha sido creada con exito!');
				
				args.win1.close();
				$.win.close();
				$.win.animate({
					top : "100%"
				});
			}
		}
	);
});
