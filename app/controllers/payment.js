var args = arguments[0] || {};

init();

function init() {
    
    //$.paymentWay.setUrl("www.warever.es/pay?id=" +args.order_id);
	$.paymentWay.setUrl("http://www.warever.es/pay/" + args.order_id);
}

$.paymentWay.addEventListener('load', function(){
	var newUrl = $.paymentWay.getUrl();
	
	if(newUrl == "http://www.warever.es/pay-result?result=0"){
		alert('Lo sentimos ha ocurrido un error durante el pago, por favor inténtalo de nuevo.');
		$.win.close();
	}else if(newUrl == "http://www.warever.es/pay-result?result=1"){
		var next = Alloy.createController("thanks", {delivery_code : args.delivery_code});
		next = next.getView();
		winClose.push(next);
		next.open();
		next.animate({left : 0});
	} else {
		Ti.API.info('else');
	}
});
