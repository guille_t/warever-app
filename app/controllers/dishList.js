var args = arguments[0] || {};

Ti.API.info(args);

var bbdd = require('bbdd');

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle(args.name); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
	
	setdishRow(args.products);
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	winClose.push(next);
	next.open();
	next.animate({left : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win); 
	$.win.animate({left : "100%"});
});

//table
function calcImageSize() {
    var HEIGHT_PERCENTAGE = 66.56;
    var width = Ti.Platform.displayCaps.platformWidth;
    var percent = width * 0.01;
    var height = HEIGHT_PERCENTAGE * percent;
    if (OS_ANDROID) {
        height /= Ti.Platform.displayCaps.logicalDensityFactor;
    }
    return height;
}

//Vendor Views
function setdishRow(products) {

    var imageHeight = calcImageSize();
    imageHeight += "dp";

    for (var i in products) {
    	
        var row = Ti.UI.createTableViewRow({
            top : 0,
            width : Ti.Platform.displayCaps.platformWidth,
            height : imageHeight,
            _data : products[i]
        });
        
         var activityIndicator = Ti.UI.createActivityIndicator({
		  color: Alloy.CFG.appColor,
		  font: {
		  	fontFamily: Alloy.CFG.boldFont, 
		  	fontSize:"16dp"
		  },
		  style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
		  top : "20dp",
		  message: 'Cargando...',
		  height:Ti.UI.SIZE,
		  width:Ti.UI.SIZE
		});

        var image = Ti.UI.createImageView({
        	defaultImage : "/images/noDispo.png",
            image : products[i].image,
            zIndex : -1,
            //height : imageHeight,
            width : "100%",
        	loaded : false,
            aI : activityIndicator
        });

		if(image.loaded == false){
			image.aI.show();
		}
	
		image.addEventListener('load', function(){
			this.loaded = true;
			this.aI.hide();
		});

        var name = Ti.UI.createLabel({
            top : "20dp",
            left : "12dp",
            width : "auto",
            height : 'auto',
            borderRadius : 4,
            backgroundColor : "#6000",
            zIndex : 1,
            textAlign : "center",
            font : {
                fontFamily : Alloy.CFG.boldFont,
                fontSize : "18dp"
            },
            shadowColor : "#000000",
            shadowRadius : 4,
            shadowOffset : {
                x : 1,
                y : 1
            },
            text : products[i].name,
            color : "white"
        });
        
        var price = Ti.UI.createLabel({
            bottom : "20dp",
            right : "12dp",
            width : "auto",
            height : 'auto',
            borderRadius : 4,
            backgroundColor : "#CC5a88af",
            zIndex : 1,
            textAlign : "center",
            font : {
                fontFamily : Alloy.CFG.boldFont,
                fontSize : "18dp"
            },
            shadowColor : "#000000",
            shadowRadius : 4,
            shadowOffset : {
                x : 1,
                y : 1
            },
            text : products[i].price,
            color : "white"
        });
		
		row.add(activityIndicator);
        row.add(image);
        row.add(name);
        row.add(price);

        $.dishTable.appendRow(row);
    }
    
    
}

$.dishTable.addEventListener('click', function(e) {
    var next = Alloy.createController("dish", e.row._data);

    next = next.getView();
    next.open();
    next.addEventListener('close', function reBot(){
		$.botBut.setBottomExport(putCartBottom(order));
		Ti.API.info('pasa por win');
	});
    winClose.push(next);
    next.animate({
        left : 0
    });
});
