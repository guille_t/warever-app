var args = arguments[0] || {};

var bbdd = require('bbdd');

var discount;
var totalPriceDiscount = args.priceDisc;

init();
 
function init() {
    $.header.setLeftButton("\ue600");
    $.header.setTitle("Descuentos");
    $.header.setTitleImage(undefined);
    $.header.setRightButton("\ue60a");
    $.header.setBackgroundColor("#FFF");
    $.header.headerTitle.setColor("#565656");

}

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win);
	$.win.animate({top : "100%"});
});


var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	next.open();
	next.animate({left : 0});
});


$.shadow.addEventListener('singletap', function(){
	
	new bbdd({},
	    'discount?vendor='+order[0].vendor_id+'&total=' +args.priceNoDisc + '&code=' + $.codeField.value, function(data){
	    	discount = data;
	    	if(discount.discount.has_discount == false){
	    		alert('El código introducido no es válido');
	    		totalPriceDiscount = args.priceDisc;
	    	}else if(discount.discount.type == "P"){
	    		var a = Titanium.UI.createAlertDialog({
				    title: '¡Felicidades!',
				    message: 'Tienes un descuento del '+discount.discount.discount + '%. Podrás ver el precio final más adelante.'
				});
				totalPriceDiscount = Number(args.priceNoDisc) - Number(args.priceNoDisc * discount.discount.discount)/100;
				a.show();
	    	}else{
	    		var a = Titanium.UI.createAlertDialog({
				    title: '¡Felicidades!',
				    message: 'Tienes un descuento de '+discount.discount.discount + '€. Podrás ver el precio final más adelante.'
				});
				totalPriceDiscount = Number(args.priceNoDisc) - Number(discount.discount.discount);
				a.show();
	    	}
	}, 'GET');
	Ti.API.info(totalPriceDiscount);
});

$.botBut.addEventListener('singletap', function(){
	var next = Alloy.createController("deliver", {priceDiscount: totalPriceDiscount, shippingPrice : args.shippingPrice, code_discount: $.codeField.value});
	next = next.getView();
	next.open();
	winClose.push(next);
	next.animate({left : 0});
});

//setDiscount
function setDiscount(discount){
	if(discount.discount.has_discount == false){
		$.discountText.setText('Vaya, no existen descuentos aplicables.');
		$.discount.visible = false;
		$.discountLogo.visible = true;
	} else if(discount.discount.type == "P"){
		$.discountText.setText('Tienes un descuento del:');
		$.discount.setText(discount.discount.discount + '%');
		$.discount.visible = true;
		$.discountLogo.visible = false;
	} else {
		$.discountText.setText('Tienes un descuento de:');
		$.discount.setText(discount.discount.discount + '€');
		$.discount.visible = true;
		$.discountLogo.visible = false;
	}
}
