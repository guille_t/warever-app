var args = arguments[0] || {};

init();

function init() {
    $.header.setLeftButton("\ue60f");
    $.header.setTitle("Local del pedido");
    $.header.setTitleImage(undefined);
    $.header.setLeftButtonColor("#FFFFFF");
    $.header.setBackgroundColor(Alloy.CFG.appColor);
    $.header.setRightButtonText("Hecho");
    $.header.setRightButton(undefined);
    $.header.setRightButtonColor("#FFFFFF");
}

var leftButton = $.header.getLeftButton();
var rightButton = $.header.getRightButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});

rightButton.addEventListener('singletap', function() {
	if ($.localNameT.value == '' || $.localCityT.value == '') {
		alert("Existen campos obligatorios, por favor, rellenalos.");
		return;
	}
	var local = {
		name: $.localNameT.value,
		street: $.localStreetT.value,
		city: $.localCityT.value,
		post: $.localPostT.value
	};
	args.callback(local);
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});
