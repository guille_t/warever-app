var args = arguments[0] || {};

var bbdd = require('bbdd');

var vendors = [];

init();

function init() {
	$.header.setTitleImage(undefined);
    $.header.setTitle(args.name); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
    
    $.indicator.show();
    
    new bbdd({},
        'vendors/'+ args.vendor_id, function(data){
        	if(order.length<2){
	        vendorSelected = data.vendors;
	        Ti.API.info(vendorSelected);
	        $.restaurantImage.setImage(vendorSelected.image);
	        $.indicator.hide();
	       } else {
	       	var vendorTemporal = data.vendors;
	       	$.restaurantImage.setImage(vendorTemporal.image);
	        $.indicator.hide();
	       }
    }, 'GET');
    
    $.dishImage.setImage(args.image);
    $.dishName.setText(args.name);
    $.dishText.setText(args.text);
    $.dishPrice.setText(args.price);
    
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	winClose.push(next);
	next.open();
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win);
	$.win.animate({left : "100%"});
});

$.more.addEventListener('singletap', function(){
	var add = $.number.text;
	var plus = Number(add) +1;
	$.number.setText(plus);
});

$.less.addEventListener('singletap', function(){
	var add = $.number.text;
	if(add !=1){
		var plus = Number(add)-1;
		$.number.setText(plus);
	}
});

$.add.addEventListener('singletap', function(){
	var number = Number($.number.text);
	var toCart = {name : args.name, quantity : number, product_id : args.id, vendor_id : args.vendor_id, price : args.price};
	var result = setCart(toCart);
	$.botBut.setBottomExport(result);
});
