var args = arguments[0] || {};

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Pedido realizado"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton(undefined);
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
    
    $.delivery_code.setText(args.delivery_code);
    
    order = [];
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login", {win1: args.win1, win2: args.win2, win3: args.win3, win4: $.win});
	next = next.getView();
	next.open();
	next.animate({top : 0});
});


$.goto.addEventListener('singletap', function(){
	var next = Alloy.createController("index");
	next = next.getView();
	next.open();
	next.animate({left : 0});
});
