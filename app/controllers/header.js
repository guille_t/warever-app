var args = arguments[0] || {};

exports.setTitle = function(title) {
    $.headerTitle.text = title;
};

exports.setLeftButton = function(text){
	$.leftButtonImg.text = text;
};

exports.setRightButton = function(text){
	$.rightButtonImg.text = text;
};

exports.setCallback = function setCallback(callback) {
	$.leftButton.addEventListener('singletap', callback);
};

exports.setTitleImage = function(imagePath) {
    $.headerImage.image = imagePath;
};

exports.setRightButtonText = function(text){
	$.rightButtonText.text = text;
};

exports.setBackgroundColor = function(backgroundColor){
	$.header.backgroundColor = backgroundColor;
};

exports.setLeftButtonColor = function(color){
	$.leftButtonImg.color = color;
};
exports.setRightButtonColor = function(color){
	$.rightButtonImg.color = color;
};
exports.setRightButtonColor = function(color){
	$.rightButtonText.color = color;
};

exports.enableLeftButton = function() {
    $.leftButton.touchEnabled = true;
    $.leftButton.visible = true;
};

exports.disableLeftButton = function() {
    $.leftButton.touchEnabled = false;
    $.leftButton.visible = false;
};

exports.enableRightButton = function() {
    $.rightButton.touchEnabled = true;
    $.rightButton.visible = true;
};

exports.disableRightButton = function() {
    $.rightButton.touchEnabled = false;
    $.rightButton.visible = false;
};

exports.getLeftButton = function() {
    return $.leftButton;
};

exports.getRightButton = function() {
    return $.rightButton;
};

exports.addLeftEvent = function(eventName, callback) {
    $.leftButton.addEventListener(eventName, callback);
};

exports.addRightEvent = function(eventName, callback) {
    $.rightButton.addEventListener(eventName, function() {
        for (var i = 0; i < Alloy.Globals.windows.length; i++) {
            Alloy.Globals.windows[i].close();
        }
        Alloy.Globals.windows = [];
        if (callback) {
            callback();
        }
    });
};

