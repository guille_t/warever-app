var args = arguments[0] || {};

$.win.addEventListener('focus', function reBot(){
	$.botBut.setBottomExport(putCartBottom(order));
});

var categorySelected = {id : ""};

init();

function init() {
	
	$.vendorsTable.setData([]);
	$.vendorsTable.visible = true;
	$.notYet.visible = false;
	
	new bbdd({},
        'vendors?postal_code='+args.postal_code+'&sector='+args.sector.id+'&category='+categorySelected.id, function(data){
        setVendorRow(data.vendors);
        
    }, 'GET');
    
    $.indicator.show();
	
    $.header.setTitleImage(undefined);
    $.header.setTitle(args.sector.name);
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
    
    $.botBut.setBottomExport(putCartBottom(order));
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	next.open();
	winClose.push(next);
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win); 
	$.win.animate({left : "100%"});
});

//table
function calcImageSize() {
    var HEIGHT_PERCENTAGE = 66.56;
    var width = Ti.Platform.displayCaps.platformWidth;
    var percent = width * 0.01;
    var height = HEIGHT_PERCENTAGE * percent;
    if (OS_ANDROID) {
        height /= Ti.Platform.displayCaps.logicalDensityFactor;
    }
    return height;
}

//Vendor Views
function setVendorRow(vendors) {
	
	if(vendors == 0){
		$.vendorsTable.visible = false;
		$.notYet.visible = true;
		
	} else{

	    var imageHeight = calcImageSize();
	    imageHeight += "dp";
	
	    for (var i in vendors) {
	    	
	        var row = Ti.UI.createTableViewRow({
	            top : 0,
	            width : Ti.Platform.displayCaps.platformWidth,
	            height : imageHeight,
	            _data : vendors[i]
	        });
	        
	        var activityIndicator = Ti.UI.createActivityIndicator({
			  color: Alloy.CFG.appColor,
			  font: {
			  	fontFamily: Alloy.CFG.boldFont, 
			  	fontSize:"16dp"
			  },
			  style : OS_IOS ? Titanium.UI.iPhone.ActivityIndicatorStyle.DARK : Ti.UI.ActivityIndicatorStyle.DARK,
			  top : "20dp",
			  message: 'Cargando...',
			  height:Ti.UI.SIZE,
			  width:Ti.UI.SIZE
			});
			
	        var image = Ti.UI.createImageView({
	        	defaultImage : "/images/noDispo.png",
	            image : vendors[i].image_extra,
	            zIndex : -1,
	            //height : imageHeight,
	            width : "100%",
	            loaded : false,
	            aI : activityIndicator
	        });
	
			if(image.loaded == false){
				image.aI.show();
			}
		
			image.addEventListener('load', function(){
				this.loaded = true;
				this.aI.hide();
			});
	
	        var name = Ti.UI.createLabel({
	            bottom : "20dp",
	            left : "12dp",
	            width : "auto",
	            height : 'auto',
	            borderRadius : 4,
	            backgroundColor : "#6000",
	            zIndex : 1,
	            textAlign : "center",
	            font : {
	                fontFamily : Alloy.CFG.boldFont,
	                fontSize : "18dp"
	            },
	            shadowColor : "#000000",
	            shadowRadius : 4,
	            shadowOffset : {
	                x : 1,
	                y : 1
	            },
	            text : vendors[i].name,
	            color : "white"
	        });
			
			row.add(activityIndicator);
	        row.add(image);
	        row.add(name);
	
	        $.vendorsTable.appendRow(row);
	    }
	  }
    $.indicator.hide();
}

$.vendorsTable.addEventListener('click', function(e) {
    var next = Alloy.createController("file", e.row._data);
    next = next.getView();
    next.open();
    next.addEventListener('close', function reBot(){
		$.botBut.setBottomExport(putCartBottom(order));
	});
    winClose.push(next);
    next.animate({ left : 0 });
});

$.filterView.addEventListener('singletap', function(){
	var next = Alloy.createController("categories", {callback: setCategories, sector: args.sector.id});
    next = next.getView();
    next.open();
    next.animate({ top : "0" });
});

function setCategories(params){
	if(params == null){
		categorySelected = {id : ""};
		$.filterText.setText("Todas las categorías");
		init();
	}else{
		categorySelected = params;
		Ti.API.info(categorySelected);
		$.filterText.setText(params.name);
		init();
	}
}
