var args = arguments[0] || {};

var bbdd = require('bbdd');

var newAddress;

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Mi cuenta"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton(undefined);
    $.header.setRightButtonText("Logout");
    
    $.userText.setText(Ti.App.Properties.getObject('user').nombre);
    $.street.setText(Ti.App.Properties.getObject('user').address);
    $.telfText.setText(Ti.App.Properties.getObject('user').phone);

    new bbdd({},
        'orders/'+ Ti.App.Properties.getObject('user').id, function(data){
        setOrderRow(data.orders);
    }, 'GET');

}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	
	Ti.App.Properties.setObject('user', null);
	
	if (args.win4 != null){
		args.win4.close();
		args.win4.animate({left : "100%"});
	}
	if (args.win3 != null){
		args.win3.close();
		args.win3.animate({left : "100%"});
	}
	if (args.win2 != null){
		args.win2.close();
		args.win2.animate({left : "100%"});
	}
	if (args.win1 != null) {
		args.win1.close();
		args.win1.animate({left : "100%"});
	}
	$.win.close();
	//args.fInit();
	Alloy.Globals.mainInit();
	order = [];
	winCloseFunction();
	$.win.animate({
		left : "100%"
	});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		left : "100%"
	});
});

$.moreIcon.addEventListener('singletap', function (){
	var next = Alloy.createController("moreDeliveryA");
	next = next.getView();
	next.open();
	next.animate({top : 0});
});

//Edit User name and telf.
$.editIconU.addEventListener('singletap', function(){
	if (OS_ANDROID) {
		var textfield = Ti.UI.createTextField();
	}
	var dialog = Ti.UI.createAlertDialog({
	    title: 'Edita tu nombre:',
	    androidView: OS_IOS ? null : textfield,
	    style: OS_ANDROID ? null : Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
	    buttonNames: ['OK', 'cancel'],
	    cancel: 1
	});
	
	dialog.addEventListener('click', function(e2) {
		if (e2.index != e2.source.cancel) {
			if (OS_IOS) {
				var value = e2.text;
			} else {
				var value = this.androidView.value;
			}
			
			new bbdd({
				name: value
			}, 
			'customers/'+ Ti.App.Properties.getObject('user').id, 
			function(data) {
				
			var change = Ti.App.Properties.getObject('user');
			
			change.nombre = data.customer.name;
			Ti.App.Properties.setObject('user', change);
			
			$.userText.setText(Ti.App.Properties.getObject('user').nombre);
		
		});
			
		}
	});
	dialog.show();
});

$.editIconT.addEventListener('singletap', function(){
	if (OS_ANDROID) {
		var textfield = Ti.UI.createTextField();
	}
	var dialog = Ti.UI.createAlertDialog({
	    title: 'Edita tu teléfono:',
	    androidView: OS_IOS ? null : textfield,
	    style: OS_ANDROID ? null : Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
	    buttonNames: ['OK', 'cancel'],
	    cancel: 1
	});
	
	dialog.addEventListener('click', function(e2) {
		if (e2.index != e2.source.cancel) {
			if (OS_IOS) {
				var value = e2.text;
			} else {
				var value = this.androidView.value;
			}
			
			new bbdd({
				phone: value
			}, 
			'customers/'+ Ti.App.Properties.getObject('user').id, 
			function(data) {
				
			var change = Ti.App.Properties.getObject('user');
			
			change.phone = data.customer.phone;
			Ti.App.Properties.setObject('user', change);
			$.telfText.setText(Ti.App.Properties.getObject('user').phone);
		
		});
			
		}
	});
	dialog.show();
});

//Edit address
function setEditAddress(params) {
	
	newAddress = params.street +' ' + params.number+', '+ params.city;
	var change = Ti.App.Properties.getObject('user');
			
	change.address = newAddress;
	Ti.App.Properties.setObject('user', change);
	
	$.street.setText(Ti.App.Properties.getObject('user').address);
}

$.editIconS.addEventListener('singletap', function(){
	var next = Alloy.createController("editDeliveryA", {callback: setEditAddress});
	next = next.getView();
	next.open();
	next.animate({top : 0});
});


//OrdersList Fill

function setOrderRow(order){
    for (var i in order) { 
        
        var t = order[i].created_at.split(/[- :]/);
        var formatedDate = (t[2]+'/'+t[1]+'/'+t[0]);
        
        var view1 = Ti.UI.createView({
            top : "6dp",
            //width : "100%",
            layout : "horizontal",
            height: "20dp"
        });
        //first line
        var date = Ti.UI.createLabel({
            left : "14dp",
            width : "24%",
            minimumFontSize : "12dp",
            font : {
                fontFamily : Alloy.CFG.regularFont,
                fontSize : "15dp"
            }, 
            text : formatedDate,
            color : Alloy.CFG.appColor,
            
        });   
        
        var localOrder = Ti.UI.createLabel({
            width : "50%",
            minimumFontSize : "12dp",
            font : {
                fontFamily : Alloy.CFG.regularFont,
                fontSize : "16dp"
            }, 
            text : order[i].vendor_name,
            color : Alloy.CFG.appColor6
        });
        
        var statusOrder = Ti.UI.createLabel({
            top : 0,
            right : 0,
            width : "22%",
            //minimumFontSize : "10dp",
            font : {
                fontFamily : Alloy.CFG.regularFont,
                fontSize : "12dp"
            }, 
            text : order[i].status_name,
            color : Alloy.CFG.appColor4
        });
            
         var row = Ti.UI.createTableViewRow({
            height: Ti.UI.SIZE
         });
         
          var view2 = Ti.UI.createView({
	            top : "22dp",
	            //width : "100%",
	            layout : "vertical",
	            height: Ti.UI.SIZE
        	});
         
         var code = Ti.UI.createLabel({
                font : {
                    fontFamily : Alloy.CFG.boldFont,
                    fontSize : "14dp"
                }, 
                text : 'Código de entrega: ' + order[i].delivery_code ,
                color : Alloy.CFG.appColor4,
                height: Ti.UI.SIZE,
                bottom: '2dp',
                left: '20dp',
                textAlign: 'center'
            });
         
         var list = Ti.UI.createView({
             top : "2dp",
             left: '20dp',
             height: Ti.UI.SIZE,
             layout: 'vertical'
         });
               
         for(var j in order[i].lines){
            var label = Ti.UI.createLabel({
                font : {
                    fontFamily : Alloy.CFG.regularFont,
                    fontSize : "14dp"
                }, 
                text : order[i].lines[j].product_name + ' ('+ order[i].lines[j].quantity + ')',
                color : Alloy.CFG.appColor2,
                height: Ti.UI.SIZE,
                bottom: '5dp',
                left: 0,
                textAlign: 'left'
            });
            list.add(label);
        }
        
        view2.add(list);
        view2.add(code);
        view1.add(date);
        view1.add(localOrder);
        row.add(view1);
        row.add(view2);
        row.add(statusOrder);
            
        $.ordersTable.appendRow(row);
    }
}

