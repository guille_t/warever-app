var args = arguments[0] || {};

Ti.API.info(args);

var newAddress;

var selectedAddress = Ti.App.Properties.getObject('user').address;
var selectedAddressId = Ti.App.Properties.getObject('user').address_id;
var selectedAddresCP = Ti.App.Properties.getObject('user').postal_code;

var data = [{id :0}];

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Dirección de entrega"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
    
    if(args.order){
    	$.observationView.visible = false;
    }else{
    	$.observationView.visible = true;
    }
    
    new bbdd({},
	    'customers/'+ Ti.App.Properties.getObject('user').id + '/addresses', function(data){
	    	setOtherAddresses(data.addresses);
	}, 'GET');
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next =Alloy.createController("login");
	next = next.getView();
	next.open();
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win);
	$.win.animate({
		left : "100%"
	});
});

function setNewAddress(params) {
	newAddress = params;
	setRow(newAddress);
}

function setRow(newAddress){
	var row = Ti.UI.createTableViewRow({
		top : "1%",
		height : "34dp",
		backgroundColor : Alloy.CFG.appColorBackground
	});
	var icon = Ti.UI.createLabel({
		left : 0,
		width : "15%"
	});
	var text = Ti.UI.createLabel({
		width : "70%",
		text : newAddress.street + " " + newAddress.number + ", " + newAddress.post + ' ' + newAddress.city,
		color : Alloy.CFG.appColor6,
		font : {
			fontFamily: Alloy.CFG.regularFont,
			fontSize : "14dp"
		}
	});	
	var tick = Ti.UI.createLabel({
		id : "tick",
		right : 0,
		width : "15%",
		text : "\ue602",
		color : Alloy.CFG.appColor5,
		font : {
			fontFamily: Alloy.CFG.iconFont,
			fontSize : "22dp"
		},
		textAlign : "center"
	});
	
	row.add(icon);
	row.add(text);
	row.add(tick);
	row._tick = tick;
	row._address = newAddress.street + " " + newAddress.number +", " + newAddress.city + ' ' + newAddress.post;
	row._address_id = newAddress.address_id;
	row._address_cp = newAddress.post;
	selectedAddress++;
    $.table.insertRowBefore($.table.sections[0].rows.length - 1, row);
    paint(row);
   
}

$.table.addEventListener('singletap', function(e) {
	if (e.row._tick) {
		paint(e.row);
		e.row._tick.setColor(Alloy.CFG.appColor);
	} else {
		var next = Alloy.createController("newDeliveryA", {callback: setNewAddress});
	    next = next.getView();
	   	next.open();
	    next.animate({top : 0});
	}
});

$.bottom.addEventListener('singletap', function(e) {
	
	if(!selectedAddress){
		var a = Titanium.UI.createAlertDialog({
			    title: 'Oops!',
			    message: 'Debes seleccionar dónde quieres que te entreguemos el pedido.'
			});
			a.show();
		
	}else if(!args.order){
		for(var i in vendorSelected.postal_codes_deal){
			var weCan = false;
			
			if(Number(vendorSelected.postal_codes_deal[i]) === Number(selectedAddressCP)){
				weCan = true;
				break;
			}
		}
			if(weCan){
				var next = Alloy.createController("summary", {house: selectedAddress, address_id: selectedAddressId, comments: $.observationText.value, priceDiscount: args.priceDiscount, shippingPrice : args.shippingPrice, code_discount: args.code_discount });
			     next = next.getView();
			     next.open();
			     winClose.push(next);
			     next.animate({left : 0});
			} else {
				var a = Titanium.UI.createAlertDialog({
				    title: 'Lo sentimos',
				    message: 'Este restaurante no envía a la dirección seleccionada.'
				});
				a.show();
				}
	} else {
	     var next = Alloy.createController("summaryWarever", {order: args.order, local: args.local, house: selectedAddress, address_id: selectedAddressId});
	     next = next.getView();
	     winClose.push(next);
	     next.open();
	     next.animate({left : 0});
    }
});

function paint(current_row) {
	for (var i in $.table.sections[0].rows) {
		var row = $.table.sections[0].rows[i];
		if (row._tick) {
			row._tick.color = Alloy.CFG.appColor5;
		}
		if (current_row == row) {
			row._tick.color = Alloy.CFG.appColor;
			selectedAddress = row._address;
			selectedAddressId = row._address_id;
			selectedAddressCP = row._address_cp;
		}
	}
}

//setOtherAddresses
function setOtherAddresses(addresses){
	for(var i in addresses){
		var row = Ti.UI.createTableViewRow({
			top : "1%",
			height : "34dp",
			backgroundColor : Alloy.CFG.appColorBackground,
			_data : addresses[i]
		});
		var icon = Ti.UI.createLabel({
			left : 0,
			width : "15%",
			text : "\ue60b",
			color : Alloy.CFG.appColor,
			font : {
				fontFamily: Alloy.CFG.iconFont,
				fontSize : "22dp"
			},
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER
		});
		var text = Ti.UI.createLabel({
			width : "70%",
			text : addresses[i].address + " " + addresses[i].number + ", " + addresses[i].postal_code + " " + addresses[i].city,
			color : Alloy.CFG.appColor6,
			font : {
				fontFamily: Alloy.CFG.regularFont,
				fontSize : "14dp"
			}
		});	
		var tick = Ti.UI.createLabel({
			id : "tick",
			right : 0,
			width : "15%",
			text : "\ue602",
			color : Alloy.CFG.appColor5,
			font : {
				fontFamily: Alloy.CFG.iconFont,
				fontSize : "22dp"
			},
			textAlign : "center"
		});
		
		row.add(icon);
		row.add(text);
		row.add(tick);
		row._tick = tick;
		row._address = addresses[i].address + " " + addresses[i].number + ", " + addresses[i].postal_code  + ' ' + addresses[i].city;
		row._address_id = addresses[i].id;
		row._address_cp = addresses[i].postal_code;
		selectedAddress++;
	    $.table.insertRowBefore($.table.sections[0].rows.length - 1, row);
	   
	  }
}