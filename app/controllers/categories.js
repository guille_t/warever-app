var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

var selectedCategory;

function init() {
    
    $.indicator.show();
    
    $.notYet.visible = false;
    
     new bbdd({},
        'vendors-categories?sector=' + args.sector, function(data){
        	setCategories(data.categories);
    }, 'GET');
   
}

$.leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({left : "100%"});
});

$.rightButton.addEventListener('singletap', function(){
	args.callback(selectedCategory);
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});

function setCategories(categories){
	if(categories == 0){
		$.categoriesTable.visible = false;
		$.notYet.visible = true;
		
	} else{
	
		for(var i in categories){
	        
	        var name = Ti.UI.createLabel({
	            left : "12dp",
	            width : "70%",
	            font : {
	                fontFamily : Alloy.CFG.regularFont,
	                fontSize : "14dp"
	            }, 
	            text : categories[i].name,
	            color : Alloy.CFG.appColor3
	        });   
	        
	        var tick = Ti.UI.createLabel({
	        	id : "tick",
				right : 0,
				width : "15%",
				text : "\ue602",
				color : Alloy.CFG.appColor5,
				font : {
					fontFamily: Alloy.CFG.iconFont,
					fontSize : "22dp"
				},
				textAlign : "center"
	        });
	        
	         var row = Ti.UI.createTableViewRow({
	            height : "42dp",
	            _data : categories[i]
	         });
	       
	
	        row.add(name);
	        row.add(tick);
	        
	        row._tick = tick;
	        
	        $.categoriesTable.appendRow(row);
			$.categoriesTable.setHeight(Ti.UI.SIZE);
		}
	}
	
	$.indicator.hide();
}

$.categoriesTable.addEventListener('singletap', function(e) {
	paint(e.row);
	e.row._tick.setColor(Alloy.CFG.appColor);
});

function paint(current_row) {
	for (var i in $.categoriesTable.sections[0].rows) {
		var row = $.categoriesTable.sections[0].rows[i];
		if (row._tick) {
			row._tick.color = Alloy.CFG.appColor5;
		}
		if (current_row == row) {
			row._tick.color = Alloy.CFG.appColor4;
			selectedCategory = row._data;
		}
	}
}