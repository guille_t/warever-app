var args = arguments[0] || {};

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Condiciones del servicio"); 
    $.header.headerTitle.setColor("#FFFFFF");
    $.header.setBackgroundColor(Alloy.CFG.appColor4);
    $.header.setLeftButton(undefined);
    $.header.setRightButton("\ue602");
    $.header.setRightButtonText(undefined);
    $.header.rightButtonImg.setColor("#FFFFFF");
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});
