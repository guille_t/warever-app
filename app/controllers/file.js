var args = arguments[0] || {};

var bbdd = require('bbdd');

var categoryRest;

init();

$.win.addEventListener('focus', function reBot(){
	$.botBut.setBottomExport(putCartBottom(order));
});

function init() {
    $.header.setLeftButton("\ue600");
    $.header.setTitle(args.name);
    $.header.setTitleImage(undefined);
    $.header.setRightButton("\ue60a");
    $.header.setBackgroundColor("#FFF");
    $.header.headerTitle.setColor("#565656");
    
    if(args.is_open == true){
    	$.status.setText("\ue625");
    	$.status.setColor("#00b369");
    }
    
    $.indicator.show();
    
    $.restIcon.setImage(args.image);
    
     new bbdd({},
        'vendors-categories/'+ args.category_id, function(data){
        	categoryRest = data.category.name;
        $.type.setText(data.category.name);
    }, 'GET');
    
    new bbdd({},
        'vendors/'+ args.id +'/products-categories', function(data){
        fillList(data.categories);
    }, 'GET');

	for(var j = 0; j <5; j++ ){
	        	
    	var starColor = Alloy.CFG.appColor;
    	
    	if(Number(args.rating) <= j){
    		starColor = "#a8bfd3";
    	}
    	
    	var star = Ti.UI.createLabel({
    		height: Ti.UI.SIZE,
    		left: "2dp",
            bottom: '5dp',
            font : {
                fontFamily : Alloy.CFG.iconFont,
                fontSize : "18dp"
            }, 
            text : "\ue61d",
            color : starColor
    	});
    	
    	$.stars.add(star);	 
    	$.stars.setWidth(Ti.UI.SIZE);     	
	  }

}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	winClose.push(next);
	next.open();
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win);
	$.win.animate({top : "100%"});
	$.win.addEventListener('focus', function reBot(){
	$.botBut.setBottomExport(putCartBottom(order));
	Ti.API.info('pasa por win');
});
});

// $.share.addEventListener('singletap', function(){
	// Titanium.Platform.openURL(args.url);
// });

$.moreInfo.addEventListener('singletap', function(){
	var next = Alloy.createController("moreInfo", {vendor: args, categoryRest: categoryRest});
	next = next.getView();
	next.open();
	next.animate({top : 0});
});

$.opinions.addEventListener('singletap', function(){
	var next = Alloy.createController("opinion", {vendor: args});
	next = next.getView();
	next.open();
	next.animate({top : 0});
});

//List Fill
function fillList(categories){
	
	for(var i in categories){
		
	         var row = Ti.UI.createTableViewRow({
	            height : "44dp",
            	_data : categories[i]
	            
	         });
		
	        var title = Ti.UI.createLabel({
	            left : "16dp",
				font : {
					fontFamily : Alloy.CFG.boldFont,
					fontSize : "16dp"
				}, 
				color: "#000",
				text : categories[i].name
	        });   
	        
	         
	        var arrow = Ti.UI.createLabel({
	            right : "6dp",
	            width : "14%",
				text : "\ue62c",
				font : {
					fontFamily : Alloy.CFG.iconFont,
					fontSize : "18dp"
				}, 
				color: Alloy.CFG.appColor2,
				textAlign : "center"
	        });
	        
	        
	        row.add(title);
	        row.add(arrow);
	            
	        $.restaurantList.appendRow(row);
			$.restaurantList.setHeight(Ti.UI.SIZE);
	}
	$.indicator.hide();
}

$.restaurantList.addEventListener('click', function(e) {
    var next = Alloy.createController("dishList", e.row._data);
    next = next.getView();
    next.open();
    next.addEventListener('close', function reBot(){
		$.botBut.setBottomExport(putCartBottom(order));
		Ti.API.info('pasa por win');
	});
    winClose.push(next);
    next.animate({
        left : 0
    });
});
