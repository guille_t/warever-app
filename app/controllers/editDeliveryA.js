var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

function init() {
    $.header.setLeftButton("\ue60f");
    $.header.setTitle("Cambio de dirección");
    $.header.setTitleImage(undefined);
    $.header.setLeftButtonColor("#FFFFFF");
    $.header.setBackgroundColor(Alloy.CFG.appColor4);
    $.header.setRightButtonText("Hecho");
    $.header.setRightButton(undefined);
    $.header.setRightButtonColor("#FFFFFF");
}

var leftButton = $.header.getLeftButton();
var rightButton = $.header.getRightButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		top : "100%"
	});
});

rightButton.addEventListener('singletap', function() {

	var house = {
		street: $.newStreetT.value,
		number: $.newNumberT.value,
		city: $.newCityT.value,
		post: $.newPostT.value,
		address_id : Ti.App.Properties.getObject('user').id
	};
	
	new bbdd({
		address: house.street, 
		number: house.number,
		postal_code: house.post, 
		city: house.city, 
		name: Ti.App.Properties.getObject('user').nombre, 
		phone: Ti.App.Properties.getObject('user').phone
		}, 
		'customers/'+ Ti.App.Properties.getObject('user').id +'/addresses/'+Ti.App.Properties.getObject('user').address_id, 
		function(data) {
			args.callback(house);
			$.win.close();
			$.win.animate({
				top : "100%"
			});
		}
	);
});
