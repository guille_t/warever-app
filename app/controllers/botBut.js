 var args = arguments[0] || {};

init();

function init(){
	
	setBottom(putCartBottom(order));
    
}

function setBottom(a){
	if(a.additionProducts==undefined){
		$.numberItem.setText('¡Vaya!');
		$.priceItem.setText('Está vacío');
	} else if (a==1){
		$.numberItem.setText(a.additionProducts + ' producto');
		$.priceItem.setText(a.additionPrice + ' €');
	}else{
		$.numberItem.setText(a.additionProducts + ' productos');
		$.priceItem.setText(a.additionPrice + ' €');
	}
}

exports.setBottomExport = setBottom;

	

$.botBut.addEventListener('singletap', function(){
	
	if(order.length <1){
		var a = Titanium.UI.createAlertDialog({
		    title: '¡Vaya!',
		    message: 'Tu cesta está vacía, añade algo para acceder.'
		});
		a.show();
	} else {
		var next = Alloy.createController("cart");
		next = next.getView();
		next.addEventListener('close', function closeSetBottom (){
			setBottom(putCartBottom(order));
			});
		next.open();
		winClose.push(next);
		next.animate({top : 0});
	}
});