var args = arguments[0] || {};

if (args.name) {
	setLocal(args);
	$.ins.visible = true;
	$.ins.touchEnabled = true;
	
	$.ins.addEventListener('singletap', function(e) {
    	 Titanium.Platform.openURL(args.url);
	});
} 

var data = [{
    id: 0,
}];

var win = $.win;
var order = [];
var local;

init();

function init() {
	setElements(data);
    $.header.setTitleImage(undefined);
    $.header.setTitle("Pide lo que quieras"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);
    
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	next.open();
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		left : "100%"
	});
});

function setLocal(params) {
	local = params;
	setAddressView(local);
}

$.where.addEventListener('singletap', function(e) {
        
     var next = Alloy.createController("where", {callback: setLocal});
       
     next = next.getView();
     next.open();
     next.animate({top : 0});
});

function setElements(data) {
    
    for (var i = 0; i < data.length; i++) {
		var row = Ti.UI.createTableViewRow({
			height : "48dp"
		});
		
		var label = Ti.UI.createLabel({
			left: '14dp',
			height : "100%",
			width : "66%",
			text : 'Escribe aquí',
			font :{
				fontFamily: Alloy.CFG.regularFont,
				fontSize : "14dp"
			},
			_i:'name'
		});
		
		label.addEventListener("click", function(e){
			showAlert(e);
		});
		
		var viewPrice = Ti.UI.createView({
			right: '64dp',
			height : "34dp",
			width : "12%",
			backgroundColor : Alloy.CFG.appColor5,
			borderRadius : 2
		});
		
		viewPrice.addEventListener("click", function(e){
			showAlert(e);
		});
		
		var labelPriceSym = Ti.UI.createLabel({
			right: '4dp',
			width : "22%",
			color : Alloy.CFG.appColor,
			font :{
				fontFamily: Alloy.CFG.boldFont,
				fontSize : "14dp"
			},
			text : "€"
		});
		
		var labelPrice = Ti.UI.createLabel({
			left: '0',
			height : "100%",
			width : "78%",
			color : Alloy.CFG.appColor,
			font :{
				fontFamily: Alloy.CFG.boldFont,
				fontSize : "14dp"
			},
			text : "0",
			_i: 'price',
			textAlign : "center"
		});
		
		var viewQuant = Ti.UI.createView({
			right: '14dp',
			height : "34dp",
			width : "12%",
			backgroundColor : Alloy.CFG.appColor5,
			borderRadius : 2
		});
		
		var labelQuantSym = Ti.UI.createLabel({
			left: '4dp',
			width : "22%",
			color : Alloy.CFG.appColor,
			font :{
				fontFamily: Alloy.CFG.boldFont,
				fontSize : "14dp"
			},
			text : "x"
		});
		
		var labelQuant = Ti.UI.createLabel({
			right: '0',
			height : "100%",
			width : "78%",
			color : Alloy.CFG.appColor,
			font :{
				fontFamily: Alloy.CFG.boldFont,
				fontSize : "14dp"
			},
			_i: 'num',
			textAlign : "center"
		});
		
		viewQuant.addEventListener("click", function(e){
			showAlert(e);
		});

		label._num = labelQuant;

		row.add(label);
		row.add(viewPrice);
		row.add(viewQuant);
		
		viewPrice.add(labelPriceSym);
		viewPrice.add(labelPrice);
		viewQuant.add(labelQuantSym);
		viewQuant.add(labelQuant);
		
		row._myLabel = label;
		row._myPrice = labelPrice;
		row._myNum = labelQuant;
        $.table.appendRow(row);
        
        setTotalPrice();
    }

}

var showAlert = function(e) {
	if (OS_ANDROID) {
		var textfield = Ti.UI.createTextField();
	}
	switch (e.source._i) {
		case 'num': var text = '¿Cuántos quieres?'; break;
		case 'name': var text = '¿Qué quieres?'; break;
		case 'price': var text = '¿Sabes el precio unitario?(si no lo sabes dejalo en blanco)'; break;
	}
	var dialog = Ti.UI.createAlertDialog({
	    title: text,
	    androidView: OS_IOS ? null : textfield,
	    style: OS_ANDROID ? null : Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
	    buttonNames: ['OK', 'cancel'],
	    cancel: 1
	});
	if (e.source.text != 'Escribe aquí') {
		var editing = true;
	} else {
		var editing = false;
	}
	dialog.addEventListener('click', function(e2) {
		if (e2.index != e2.source.cancel) {
			if (OS_IOS) {
				var value = e2.text;
			} else {
				var value = this.androidView.value;
			}
			if (e.source._i == 'num') {
				e.source.text = value;
				setTotalPrice();
			} else if (e.source._i == 'price') {
				e.source.text = value;
				setTotalPrice();
			} else {
				if (value) {
					e.source.text = value;
					e.source._num.text = '1';
					if (!editing) {
						setElements([1]);
					}
				}
			}
			
		}
	});
	dialog.show();
};

$.bottom.addEventListener('singletap', function() {
	order = []; 
	var rows = $.table.sections[0].rows;
	for (var i in rows) {
		if (rows[i]._myLabel.text == 'Escribe aquí') {
			continue;
		}
		
		order.push({
			product_name: rows[i]._myLabel.text,
			quantity: rows[i]._myNum.text,
			price: rows[i]._myPrice.text
		});
	}
	if (local != null && Ti.App.Properties.getObject('user') != null && order.length != 0) {

		var next = Alloy.createController("deliver", {order: order, local: local});
		next = next.getView();
		winClose.push(next);
		next.open();
		next.animate({left : 0});
		
	} else if (local == null){
		alert("Los datos del local son necesario.");
	} else if (!Ti.App.Properties.getObject('user')) {
		alert("Los datos de usuario son necesario. Accede desde la esquina superior derecha");
	} else if(order.length == 0){
		alert("¡Olvidas decirnos qué quieres pedir!");
	}
	
});

function setAddressView(local){
		var icon = Ti.UI.createLabel({
			left: 0,
			height : "70%",
			width : "20%",
			color : Alloy.CFG.appColor,
			backgroundColor : Alloy.CFG.appColorBackground,
			text : "\ue608",
			font : {
				fontFamily : Alloy.CFG.iconFont,
				fontSize : "26dp"
			}, 
			textAlign : "center"
		});
		var name = Ti.UI.createLabel({
			top: 0,
			height : "60%",
			width : "80%",
			right : 0,
			backgroundColor : Alloy.CFG.appColorBackground,
			color : Alloy.CFG.appColor6,
			text : local.name,
			font : {
				fontFamily : Alloy.CFG.regularFont,
				fontSize : "16dp"
			}, 
			verticalAlign : Titanium.UI.TEXT_VERTICAL_ALIGNMENT_BOTTOM
		});
		var where = Ti.UI.createLabel({
			right: 0,
			bottom : 0,
			height : "40%",
			width : "80%",
			backgroundColor : Alloy.CFG.appColorBackground,
			color : Alloy.CFG.appColor2,
			text : local.city,
			font : {
				fontFamily : Alloy.CFG.regularFont,
				fontSize : "12dp"
			},
			verticalAlign : Titanium.UI.TEXT_VERTICAL_ALIGNMENT_TOP
		});
		
		$.direc.add(icon);
		$.direc.add(name);
		$.direc.add(where);
}

function setTotalPrice(){
	var totalPrice = 0;	
	var rows = $.table.sections[0].rows;
	
	for (var i in rows) {
		if (rows[i]._myLabel.text == 'Escribe aquí') {
			continue;
		}
		
		var lstLetters = rows[i]._myPrice.text;;
    	var priceParse = lstLetters.replace(/\,/g,'.');

		totalPrice += priceParse*Number(rows[i]._myNum.text);
	} 
	
	$.totalNum.setText(totalPrice);
}



