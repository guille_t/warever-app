var args = arguments[0] || {};

var bbdd = require('bbdd');

var discount;
var cartPrice;
var shippingPrice;
var endDiscount; 

init();

function init() {
    $.header.setLeftButton("\ue60f");
    $.header.setTitle("Cesta de la Compra");
    $.header.setTitleImage(undefined);
    $.header.setRightButton("\ue60a");
    $.header.setBackgroundColor("#FFF");
    $.header.headerTitle.setColor("#565656");
    
    $.orderTable.setData([]);
    
    $.indicator.show();
    
	cartPrice = putCartBottom(order);
	$.discountNumber.setText(' ');
	$.totalPrice.setText(' ');

	new bbdd({},
	    'delivery-costs?type=1', function(data){
	    	shippingPrice = data.data;
	    	$.shippingCostNumber.setText(data.data +' €');
	    	setCart(order);
	    	
	    	if(order.length >= 1){
				new bbdd({},
				    'discount?vendor='+order[0].vendor_id+'&total=' + cartPrice.additionPrice, function(data){
				    	discount = data;
				    	if(discount.discount.type == "P"){
				    		$.discountNumber.setText(discount.discount.discount +'%');
				    	}else {
				    		$.discountNumber.setText(discount.discount.discount +'€');
				    	}
				    	setTotalPrice();
				}, 'GET');
			}
	
	}, 'GET');
		
	if(order.length <1){
		$.win.close();
		winDeleteFunction($.win);
		$.win.animate({ top : "100%", duration : 1000});
	}
}

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	winDeleteFunction($.win);
	$.win.animate({top : "100%"});
});

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	next.open();
	next.animate({top : 0});
});

function setCart(order){
	for(var i in order){
		
		var row = Ti.UI.createTableViewRow({
	            height : "74dp",
	            backgroundColor : Alloy.CFG.appColorBackground,
            	_data : order[i]
	         });
	          
	        var view1 = Ti.UI.createView({
	        	top : 0,
	        	height : "44dp",
	        	width : "100%"
	        });
		
	        var product = Ti.UI.createLabel({
	            left : "12dp",
	            width : "60%",
				font : {
					fontFamily : Alloy.CFG.regularFont,
					fontSize : "16dp"
				}, 
				color: Alloy.CFG.appColor3,
				text : order[i].product_name
	        });   
	        
	         
	        var quantity = Ti.UI.createLabel({
	            right : "24%",
	            width : "10%",
				font : {
					fontFamily : Alloy.CFG.regularFont,
					fontSize : "16dp"
				}, 
				color: Alloy.CFG.appColor,
				textAlign : "right",
				text : order[i].quantity
	        });
	        
	        var price = Ti.UI.createLabel({
	            right : 0,
	            width : "20%",
				font : {
					fontFamily : Alloy.CFG.regularFont,
					fontSize : "16dp"
				}, 
				color: Alloy.CFG.appColor,
				textAlign : "left",
				text : order[i].price
	        });
	        
	        var cancel = Ti.UI.createLabel({
	        	height : "100%",
	        	width : "10%",
	            right : "4dp",
				font : {
					fontFamily : Alloy.CFG.iconFont,
					fontSize : "16dp"
				}, 
				color: Alloy.CFG.appColor2,
				textAlign : "right",
				text : "\ue60f",
				row: row
	        });
	        
	        cancel.addEventListener('click', function(e){
				deleteRow(this.row._data);
				init();
			});
				        
	        var view2 = Ti.UI.createView({
	        	bottom : 0,
	        	height : "30dp",
	        	width : "100%"
	        });
	        
	        var lessB = Ti.UI.createView({
	        	height : "28dp",
	        	width : "28dp",
	        	left : "55%",
	        	backgroundColor : Alloy.CFG.appColor5,
	        	row: row
	        });
	        
	        var less = Ti.UI.createLabel({
	        	height : "90%",
	        	width : "99%",
	        	top : "0",
	        	backgroundColor : "white",
	        	color : Alloy.CFG.appColor,
				font : {
					fontFamily : Alloy.CFG.boldFont,
					fontSize : "16dp"
				},
				textAlign : "center",
				text : "-"
	        });
	        
	        lessB.addEventListener('click', function(e){
				lessItem(this.row._data);
				init();
			});
	        
	        var moreB = Ti.UI.createView({
	        	height : "28dp",
	        	width : "28dp",
	        	right : "22%",
	        	backgroundColor : Alloy.CFG.appColor5,
	        	row: row
	        });
	        
	        var more = Ti.UI.createLabel({
	        	height : "90%",
	        	width : "100%",
	        	top : "0",
	        	backgroundColor : "white",
	        	color : Alloy.CFG.appColor,
				minimumFontSize : "10dp",
				font : {
					fontFamily : Alloy.CFG.boldFont,
					fontSize : "16dp"
				},
				textAlign : "center",
				text : "+"
	        });
	        
	        moreB.addEventListener('click', function(e){
				moreItem(this.row._data);
				init();
			});
	        
	       	view1.add(product);
	        view1.add(quantity);
	        view1.add(price);
	        view1.add(cancel);
	        
	        view2.add(lessB);
	        lessB.add(less);
			moreB.add(more);
			view2.add(moreB);

	        row.add(view1);
	        row.add(view2);
	            
	        $.orderTable.appendRow(row);
			$.orderTable.setHeight(Ti.UI.SIZE);
	}
	//setTotalPrice();
	$.indicator.hide();
}

function setTotalPrice(){
	if(!cartPrice){
		$.totalPrice.setText(" ");
	} else {
		if(discount.discount.has_discount == true && discount.discount.type == "P"){
			endDiscount = (cartPrice.additionPrice * discount.discount.discount)/100;
		}else if(discount.discount.has_discount == true && discount.discount.type == "V"){
			endDiscount = cartPrice.additionPrice - discount.discount.discount;
		} else{
			endDiscount = cartPrice.additionPrice;
		}
		var end = Number(endDiscount) + Number(shippingPrice);
		
		$.totalPrice.setText(end.toFixed(2) + ' €');
					
	}
}

$.leftButton.addEventListener('singletap', function(){
	var next = Alloy.createController('file', vendorSelected);
	next = next.getView();
	next.open();
	winClose.push(next); 
	next.animate({left : 0});
});


$.next.addEventListener('singletap', function(){
	
	if (!Ti.App.Properties.getObject('user')) {
		alert("Los datos de usuario son necesario. Accede desde la esquina superior derecha");
	} else if(order.length == 0){
		alert("¡Olvidas decirnos qué quieres pedir!");
	} else if(vendorSelected.is_open == 0){
		alert("El local está cerrado en estos momentos.");
	} else{
		var next = Alloy.createController("discount", {priceNoDisc : cartPrice.additionPrice, priceDisc : endDiscount, shippingPrice : shippingPrice});
		next = next.getView();
		next.open();
		winClose.push(next);
		next.animate({left : 0});
	}
});
