var args = arguments[0] || {};

var bbdd = require('bbdd');

init();

var reviews = [];

function init() {
    $.header.setLeftButton("\ue60f");
    $.header.leftButtonImg.setColor("white");
    $.header.setTitle("Opiniones");
    $.header.setTitleImage(undefined);
    $.header.setRightButton(undefined);
    $.header.setBackgroundColor(Alloy.CFG.appColor);
    $.header.headerTitle.setColor("white");
    
    $.indicator.show();
    
    $.notYet.visible = false;
    
     new bbdd({},
        'vendors/'+ args.vendor.id +'/reviews', function(data){
        	setReviews(data.reviews);
    }, 'GET');
   
}

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({left : "100%"});
});

function setReviews(reviews){
	
	if(reviews == 0){
		$.opinionTable.visible = false;
		$.notYet.visible = true;
		
	} else{
	
		for(var i in reviews){
			
			var t = reviews[i].created_at.split(/[- :]/);
	        var formatedDate = (t[2]+'/'+t[1]+'/'+t[0]);
	        
	        var view1 = Ti.UI.createView({
	            top : "6dp",
	            height: Ti.UI.SIZE
	        });
	        //first line
	        var title = Ti.UI.createLabel({
	            left : "14dp",
	            width : "40%",
	            minimumFontSize : "12dp",
	            font : {
	                fontFamily : Alloy.CFG.regularFont,
	                fontSize : "15dp"
	            }, 
	            text : formatedDate,
	            color : Alloy.CFG.appColor4
	        });   
	        
	        var value = Ti.UI.createView({
	        	top : 0,
	        	height : Ti.UI.SIZE,
	            width : "28%",
	            layout : "horizontal",
	            right : 0
	        });
	        
	        for(var j = 0; j <5; j++ ){
	        	
	        	var starColor = Alloy.CFG.appColor;
	        	
	        	if(Number(reviews[i].value) <= j){
	        		starColor = "#a8bfd3";
	        	}
	        	
	        	var star = Ti.UI.createLabel({
	        		height: Ti.UI.SIZE,
	                bottom: '5dp',
	        		//minimumFontSize : "10dp",
		            font : {
		                fontFamily : Alloy.CFG.iconFont,
		                fontSize : "18dp"
		            }, 
		            text : "\ue61d",
		            color : starColor
	        	});
	        	
	        	value.add(star);
	        	
	        }
	        
	        
	        
	         var row = Ti.UI.createTableViewRow({
	            height : Ti.UI.SIZE
	         });
	         
	         var view = Ti.UI.createView({
	            top : "26dp",
	            height: Ti.UI.SIZE
	        }); 
	        
	        //SecondLine
	        var text = Ti.UI.createLabel({
	            bot : 0,
	            width : "90%",
	            minimumFontSize : "10dp",
	            font : {
	                fontFamily : Alloy.CFG.regularFont,
	                fontSize : "14dp"
	            }, 
	            text : reviews[i].comments,
	            color : Alloy.CFG.appColor3
	        });
	        
	        view1.add(title);
	        view1.add(value);
	        row.add(view1);
	        view.add(text);
	        row.add(view);
	            
	        $.opinionTable.appendRow(row);
			
				
		}
	}

	$.indicator.hide();
}
