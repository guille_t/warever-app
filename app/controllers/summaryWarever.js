var args = arguments[0] || {};

var bbdd = require('bbdd');

var aux = 0;

init();

function init() {
    $.header.setTitleImage(undefined);
    $.header.setTitle("Resumen del pedido"); 
    $.header.headerTitle.setColor("#565656");
    $.header.setLeftButton("\ue600");
    $.header.setRightButton("\ue60a");
    $.header.setRightButtonText(undefined);

	$.deliveryA.setText(args.house);
	
	setOrderRow(args.order);
	$.localName.setText(args.local.name +', '+ args.local.city);
	
	$.userPhone.setText(Ti.App.Properties.getObject('user').phone);
	
}

var rightButton = $.header.getRightButton();

rightButton.addEventListener('singletap', function(){
	var next = Alloy.createController("login");
	next = next.getView();
	winClose.push(next);
	next.open();
	next.animate({top : 0});
});

var leftButton = $.header.getLeftButton();

leftButton.addEventListener('singletap', function(){
	$.win.close();
	$.win.animate({
		left : "100%"
	});
});

$.bottom.addEventListener('singletap', function(){
	
	if(($.smsTick.color == Alloy.CFG.appColor || $.wappTick.color == Alloy.CFG.appColor) && $.tickCost.color ==  Alloy.CFG.appColor && $.tickTerms.color == Alloy.CFG.appColor){
		
		if($.smsTick.color == Alloy.CFG.appColor){
		    var contact = "SMS";
		}else{
		    var contact = "Whatsapp";
		}
		
		if($.soonTick.color == Alloy.CFG.appColor){
			var when = "Lo antes posible";
			var whenDate = "Lo antes posible";
		} else {
			var when = "Otra fecha / hora";
			var whenDate = $.timeText.text;
		}
		
		$.indicator.show();
		$.bottom.hide();
		
		new bbdd({
				address: args.address_id,
				vendor_name: args.local.name,
				vendor_address: args.local.street,
				vendor_postalcode: args.local.post,
				vendor_city: args.local.city,
				contact_type: contact,
				when: when,
				when_date: whenDate,
				discount_code: $.codeField.value
			}, 
			'request-product/'+ Ti.App.Properties.getObject('user').id, 
			function(data) {
				
				if(data == null || data == undefined){
					var a = Titanium.UI.createAlertDialog({
					    title: 'Oops!',
					    message: 'Algo ha fallado, por favor inténtalo de nuevo.'
					});
					a.show();
					$.indicator.hide();
					$.bottom.show();
				} else{
			
				var request_id = data.order.id;
				var delivery_code = data.order.delivery_code;
		
				var o = args.order;
				
				Ti.API.info(o);
				
				aux = 0;
				
				for (var i in o) {
					new bbdd({
						product: o[i].product_name,
						quantity: o[i].quantity,
						price: o[i].price
					}, 'request-product/'+ Ti.App.Properties.getObject('user').id +'/lines/'+ request_id, function() {
						aux ++;
						if (aux == o.length) {
							new bbdd({}, 'orders/'+ request_id +'/start', function(data) {
								Ti.API.info(data);
								$.indicator.hide();
								$.bottom.show();
							}, 'GET');
						}
					});
				}	
				
				var next = Alloy.createController("thanks", {win1: args.win1, win2: args.win2, win3: $.win, delivery_code : delivery_code});
				next = next.getView();
				next.open();
				next.animate({left : 0});
			}
		}); 
	}else{
		alert("Debes aceptar las condiciones para poder realizar el pedido.");
	}
		
});

$.cancel.addEventListener('singletap', function(){
	order = [];
	winCloseFunction();
	$.win.close();
	$.win.animate({
		left : "100%"
	});
});


function setOrderRow(order){
	for (var i = 0; i < order.length; i++) {
		
		var row = Ti.UI.createLabel({
			height : "20dp",
			width: "96%",
			left : "2%",
			font : {
				fontFamily : Alloy.CFG.regularFont,
				fontSize : "14dp"
			}, 
			text : order[i].product_name + ' ('+ order[i].quantity + ')',
			color : Alloy.CFG.appColor2
		});
			
		$.container.add(row);
	}
}


//Selection ticks
$.select.addEventListener('click', function(e){
	if(e.source == $.smsSel || e.source == $.smsTick || e.source == $.SMS){
		$.smsTick.color = Alloy.CFG.appColor;
		$.wappTick.color = Alloy.CFG.appColor5;
	}else{
		$.smsTick.color = Alloy.CFG.appColor5;
		$.wappTick.color = Alloy.CFG.appColor;
	}
});

$.cost.addEventListener('click', function(){
	if($.tickCost.color == Alloy.CFG.appColor5){
		$.tickCost.color = Alloy.CFG.appColor;
	}else{
		$.tickCost.color = Alloy.CFG.appColor5;
	}
});

$.terms.addEventListener('click', function(){
    if($.tickTerms.color == Alloy.CFG.appColor5){
        $.tickTerms.color = Alloy.CFG.appColor;
    }else{
        $.tickTerms.color = Alloy.CFG.appColor5;
    }
});

$.textTerms.addEventListener('singletap', function (){
	Titanium.Platform.openURL(Alloy.CFG.urlTerms);
});

//Select delivery time
$.timeSel.addEventListener('singletap', function(){
	if (OS_ANDROID) {
		var textfield = Ti.UI.createTextField();
	}
	var dialog = Ti.UI.createAlertDialog({
	    title: '¿Cuándo quieres recibirlo?:',
	    androidView: OS_IOS ? null : textfield,
	    style: OS_ANDROID ? null : Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
	    buttonNames: ['OK', 'cancel'],
	    cancel: 1
	});
	
	dialog.addEventListener('click', function(e2) {
		if (e2.index != e2.source.cancel) {
			if (OS_IOS) {
				var value = e2.text;
			} else {
				var value = this.androidView.value;
			}
			
			$.timeText.setText(value);
			$.timeTick.setColor(Alloy.CFG.appColor);
			$.soonTick.setColor(Alloy.CFG.appColor5);
		}
	});
	dialog.show();
});

//Paint time ticks
$.soonSel.addEventListener('click', function(){
	if($.soonTick.color == Alloy.CFG.appColor5){
		$.soonTick.color = Alloy.CFG.appColor;
		$.timeTick.color = Alloy.CFG.appColor5;
	}
});