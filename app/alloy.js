// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

var bbdd = require('bbdd');

var AppManager = {

    windows : [],

    fontMaps : {},

    initializeFont : function initializeFont(fontname) {
        if ( typeof this.fontMaps[fontname] == "undefined") {
            this.fontMaps[fontname] = {};
        }

        try {
            var obj = JSON.parse(Titanium.Filesystem.getFile('fontmaps/' + fontname + '.json').read().text);
            var fontmap = obj.icons;
            for (var i = 0; i < fontmap.length; i++) {
                var font = fontmap[i].properties.name;
                var code = fontmap[i].properties.code;
                //console.log("Found: " + fontname + " / " + font + " / " + code);
                this.fontMaps[fontname][font] = String.fromCharCode(code);
            }
        } catch (fontParseError) {
            console.log("*** There was a font parsing error.  " + "Did you copy your font's selection.json file " + "into the assets folder of your application and name it " + fontname + ".json?");
            console.log("*** fontParseError: " + fontParseError);
        }
    },


    openWindow : function openWindow(name, trackWin, data) {
        var win = Alloy.createController(name, data).getView();
        win.open({
            left : 0
        });
        if (trackWin) {
            this.windows.push(win);
        }
    },

    closeWindow : function closeWindow(win) {
        win.close({
            left : "100%"
        });
        this.windows.pop();
    },

    goToHome : function goToHome() {
        for (var i = 0; i < this.windows.length; i++) {
            this.windows[i].close();
        }
    },
    
    setMainWindow: function setMainWindow(mainWindow){
        this.mainWindow = mainWindow;
    },
     
    getWindow: function getWindow(windowName){
        var win = null;
        if(this.mainWindow){
            win = this.mainWindow[windowName];
        }
        return win;
    }
};


AppManager.initializeFont("warever");

//Windows
var winClose = [];

function winCloseFunction(){
	for(i in winClose){
		winClose[i].close();
	}
	winClose = [];
}

function winDeleteFunction(winNum){
	var index = winClose.indexOf(winNum);
	if(index != -1){
		winClose.splice(index,1);
	}
}

//Vendor
var vendorSelected; 

//Cart List
var order = []; 
var messageError = false; 

function setCart(e) {
	var insert = false;
	var refresh = false;
	if(order.length == 0) {
		insert = true;
		refresh = false;
	} else {
		if(e.vendor_id == order[0].vendor_id){
			for(var i in order){
				if(e.product_id == order[i].product_id){
					insert = false;
					refresh = true;
					break;
				} else {
					insert = true;
					refresh = false;
				}
			}
		} else {
			insert = false;
			refresh = false;
		}
	}
	
	if (insert) {
		order.push({
			vendor_id : e.vendor_id,
			product_id : e.product_id,
			product_name: e.name,
			quantity: e.quantity,
			price: e.price
		});
		messageError = false;
	} else if(refresh){
		for(var j in order){
			if(e.product_id == order[j].product_id){
				order[j].quantity += Number(e.quantity);
				break;
			}
		}
		messageError = false;
	} else {
		var a = Titanium.UI.createAlertDialog({
		    title: 'Oops!',
		    message: 'Lo sentimos, no puedes añadir productos de diferentes locales a tu cesta. Accede a ella y elimina los que no quieras.'
		});
		a.show();
		messageError = true;
	}
	return putCartBottom(order);
}

function putCartBottom(order){
	var additionProducts = 0;
	var additionPrice = 0;
	var addition = 0;
	for (var i in order){
		additionProducts = Number(additionProducts) + Number(order[i].quantity);
		additionPrice = Number(additionPrice) + (Number(order[i].price) * Number(order[i].quantity));
		addition = {additionProducts : additionProducts, additionPrice :additionPrice.toFixed(2)};
	}
	return addition;
}

function deleteRow(data){
	for(i in order){
		if(data.product_id == order[i].product_id){
			order.splice(i,1);
			break;
		}
	}
}

function lessItem(data){
	for(i in order){
		if(data.product_id == order[i].product_id){
				order[i].quantity--;
				break;
			}
	}
}

function moreItem(data){
	for(i in order){
		if(data.product_id == order[i].product_id){
				order[i].quantity++;
				break;
			}
	}
}
