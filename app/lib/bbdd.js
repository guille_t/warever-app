
module.exports = function(args, path, callback, method) {
	
	var method = method ? method : 'POST';
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info(this.responseText);
			if (!this.responseText) {
				if (callback) {
					callback(null);
				}
				return;
			}
			var result = JSON.parse(this.responseText);
			
			
			// if (result.status == 'ok') {
				// if (callback) {
					// callback(result);
				// }
			// } else {
				// if (callback) {
					// callback(null);
				// }
			// }
			if (callback) {
					callback(result);
				}
		},
		onerror: function(e) {
			Ti.API.info('Error ' + e.code);
			Ti.API.info(e);
			if (callback) {
				callback(null);
			}
		},
		timeout: 10000
	});
	
	// if (method == 'GET') {
		// args = {};
	// } else {
		// args._token = Alloy.CFG._token;
	// }
	
	Ti.API.info('-> ' + method + ' --- ' + Alloy.CFG.url + path + ' --- ' + JSON.stringify(args) + ' --- ');
	
	
	client.open(method, Alloy.CFG.url + path);
	client.setRequestHeader('X-Auth-Token', Ti.App.Properties.getString('token'));
	Ti.API.info(Ti.App.Properties.getString('token'));
	client.setRequestHeader('Authorization', ('Basic d2FyZXZlcjoxMjM3ODk='));
	client.send(args);
	
};